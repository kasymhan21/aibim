import openpyxl
import xml.dom.minidom as mindom
import pandas as pd
import operator
import time
position_contacts = ['From', "To", "Member1_ID", "Member2_ID", "Взаимодействия между сабой"]
position_persons = ['id', "Фамилия", "Имя", "Возраст"]
sheet1 = 'Contacts'
sheet2 = 'Person'
file_name = 'contacts_sample.xlsx'


def parser_data_contacts(excel_file):
    col = 1
    row = 2
    data = mindom.parse(f'small_data.xml')
    data.normalize()
    for persons in data.getElementsByTagName('Contacts'):
        for person in persons.getElementsByTagName("MapOfContact"):
            cell = excel_file.cell(row=row, column=col)
            time = person.getElementsByTagName("From")[0].childNodes[0].nodeValue
            cell.value = person.getElementsByTagName("From")[0].childNodes[0].nodeValue
            col += 1
            cell = excel_file.cell(row=row, column=col)
            time_to = person.getElementsByTagName("To")[0].childNodes[0].nodeValue
            cell.value = person.getElementsByTagName("To")[0].childNodes[0].nodeValue
            col += 1
            cell = excel_file.cell(row=row, column=col)
            cell.value = person.getElementsByTagName("Member1_ID")[0].childNodes[0].nodeValue
            col += 1
            cell = excel_file.cell(row=row, column=col)
            cell.value = person.getElementsByTagName("Member2_ID")[0].childNodes[0].nodeValue
            col += 1
            cell = excel_file.cell(row=row, column=col)
            data = pd.to_datetime(time_to) - pd.to_datetime(time)
            cell.value = data.seconds
            row += 1
            col = 1


def parser_data_persons(excel_file):
    col = 1
    row = 2
    data = mindom.parse(f'small_data.xml')
    data.normalize()
    for persons in data.getElementsByTagName('Persons'):
        for person in persons.getElementsByTagName("Member"):
            cell = excel_file.cell(row=row, column=col)
            cell.value = person.getElementsByTagName("ID")[0].childNodes[0].nodeValue
            col += 1
            cell = excel_file.cell(row=row, column=col)
            cell.value = person.getElementsByTagName("Name")[0].childNodes[0].nodeValue.split(' ')[0]
            col += 1
            cell = excel_file.cell(row=row, column=col)
            cell.value = person.getElementsByTagName("Name")[0].childNodes[0].nodeValue.split(' ')[1]
            col += 1
            cell = excel_file.cell(row=row, column=col)
            cell.value = person.getElementsByTagName("Age")[0].childNodes[0].nodeValue
            row += 1
            col = 1


def start():
    wb = openpyxl.Workbook()
    wb.create_sheet(title=sheet1, index=0)
    wb.create_sheet(title=sheet2, index=1)
    sheet = wb[sheet1]
    sheet_1 = wb[sheet2]
    for col in range(1, 6):
        cell = sheet.cell(row=1, column=col)
        cell.value = position_contacts[col - 1]
    for col in range(1, 5):
        cell = sheet_1.cell(row=1, column=col)
        cell.value = position_persons[col - 1]
    parser_data_contacts(sheet)
    parser_data_persons(sheet_1)
    wb.save(file_name)
    wb.close()


def by_the_number(contact, person):
    new, array_persons, j, i = pd.DataFrame(), {}, [], 0
    for from_time, time_to in zip(contact[position_contacts[0]], contact[position_contacts[1]]):
        data = pd.to_datetime(time_to) - pd.to_datetime(from_time)
        if data.seconds < 300:
            j.append(i)
        i += 1
    contacts = contact.drop(j)
    for i in contacts[position_contacts[2]].value_counts().to_dict():
        try:
            array_persons[i] = contacts[position_contacts[2]].value_counts().to_dict()[i] + \
                               contacts[position_contacts[3]].value_counts().to_dict()[i]
        except:
            array_persons[i] = contacts[position_contacts[2]].value_counts().to_dict()[i]
    for i in dict(sorted(array_persons.items(), key=operator.itemgetter(1))):
        new = pd.concat([new, person.loc[person[position_persons[0]] == i]])
    return new


def by_the_time(contact, person):
    new,array_persons  = pd.DataFrame(),{}
    for i in contact[position_contacts[2]].value_counts().to_dict():
        array_persons[i] = contact.loc[contact[position_contacts[2]] == i][position_contacts[4]].sum()
    for i in dict(sorted(array_persons.items(), key=operator.itemgetter(1))):
        new = pd.concat([new, person.loc[person[position_persons[0]] == i]])
    return new


def group_age(new):
    age_16 = new.loc[(new[position_persons[3]] <= 16)]
    age_16_21= new.loc[(new[position_persons[3]] >16)  & (new[position_persons[3]] <= 21)]
    age_21_55 = new.loc[(new[position_persons[3]] > 21) & (new[position_persons[3]] <= 60)]
    age_61 =new.loc[(new[position_persons[3]] > 60)]
    age = {'До 16':age_16,'от 17 до 21':age_16_21,'От 22 до 60':age_21_55,'Старше 60':age_61}
    new =''
    for i in age:
        if len(age[i]) > len(new):
            new = age[i]
    return new
if __name__ == '__main__':
    new = time.time()
    start()
    file = openpyxl.load_workbook(file_name)
    writer = pd.ExcelWriter(file_name, engine='openpyxl')
    writer.book = file
    writer.sheets = {x.title: x for x in file.worksheets}
    df = pd.ExcelFile(file_name)
    contacts = df.parse(sheet1)
    persons = df.parse(sheet2)
    the_number = by_the_number(contacts, persons)
    the_time = by_the_time(contacts, persons)
    group_age_contacts = group_age(the_number)
    the_number.to_excel(writer, sheet_name='По количеству контактов', index=False)
    the_time.to_excel(writer, 'По длительности контактов', index=False)
    group_age_contacts.to_excel(writer, 'Возрастная группа', index=False)
    writer.save()
    print(time.time()-new)
