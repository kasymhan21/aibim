import openpyxl
import re
import xml.dom.minidom as mindom
import pandas as pd

position = ['id', "Фамилия", "Имя", "Возраст"]
sheet1 = 'SMALL_DATA'
sheet2 = 'BIG_DATA'
file_name = 'sample.xlsx'


def parser_data(excel_file, name_list):
    col = 1
    row = 2
    data = mindom.parse(f'{name_list.lower()}.xml')
    data.normalize()
    for persons in data.getElementsByTagName('Persons'):
        for person in persons.getElementsByTagName("Member"):
            cell = excel_file.cell(row=row, column=col)
            cell.value = person.getElementsByTagName("ID")[0].childNodes[0].nodeValue
            col += 1
            cell = excel_file.cell(row=row, column=col)
            cell.value = person.getElementsByTagName("Name")[0].childNodes[0].nodeValue.split(' ')[0]
            col += 1
            cell = excel_file.cell(row=row, column=col)
            cell.value = person.getElementsByTagName("Name")[0].childNodes[0].nodeValue.split(' ')[1]
            col += 1
            cell = excel_file.cell(row=row, column=col)
            cell.value = person.getElementsByTagName("Age")[0].childNodes[0].nodeValue
            row += 1
            col = 1


def start():
    wb = openpyxl.Workbook()
    wb.create_sheet(title=sheet1, index=0)
    wb.create_sheet(title=sheet2, index=1)
    sheet = wb[sheet1]
    sheet_1 = wb[sheet2]
    for col in range(1, 5):
        cell = sheet.cell(row=1, column=col)
        cell.value = position[col - 1]
        cell = sheet_1.cell(row=1, column=col)
        cell.value = position[col - 1]
    parser_data(sheet, sheet1)
    parser_data(sheet_1, sheet2)
    wb.save(file_name)


def difference_to(df_sd_0, df_sd_1):
    state = []
    for i in df_sd_0[position[1]]:
        sort = df_sd_1[position[1]].isin([i])
        f = False
        for j in sort:
            if j == True:
                f = True
                pass
        if f != True:
            state.append(i)
    return df_sd_0[df_sd_0[position[1]].isin(state)].reset_index(drop=True)


def difference_2(datas):
    state = []
    for data in datas[position[1]]:
        if re.findall(r'[a-zA-Z]', data) != []:
            state.append(data)
    return datas[datas[position[1]].isin(state)].reset_index(drop=True)


def age_difference(big_data, small_data):
    data = pd.concat([small_data, big_data], ignore_index=True)
    data_sort = data.sort_values(position[1])
    state = []
    for name in data_sort[position[1]]:
        start = data_sort[data_sort[position[1]].isin([name])].reset_index(drop=True).sort_values(position[3])
        if len(start) > 1:
            xdiff = [start[position[3]][n - 1] - start[position[3]][n] for n in range(1, len(start[position[3]]))]
            fal = True
            for i in xdiff:
                if i != 10:
                    fal = False
            if fal == True:
                state.append(start)
    data = state[0]
    for i in range(1, len(state)):
        if i % 2 == 0:
            data = pd.concat([data, state[i]], ignore_index=True)
    return data


def sort_data():
    df = pd.ExcelFile(file_name)
    sheet = df.parse(sheet1)
    small_data = sheet.sort_values(position[1])
    sheet = df.parse(sheet2)
    big_data = sheet.sort_values(position[2])
    difference = difference_to(small_data, big_data)
    age = age_difference(big_data, small_data)
    search_for_english_characters = pd.concat([difference_2(small_data), difference_2(big_data)], ignore_index=True)
    with pd.ExcelWriter(file_name) as writer:
        small_data.to_excel(writer, sheet1,index=False)
        big_data.to_excel(writer, sheet2,index=False)
        difference.to_excel(writer, 'Разница между данными',index=False)
        search_for_english_characters.to_excel(writer, 'Английские символы',index=False)
        age.to_excel(writer, 'Разница в возврасте',index=False)


if __name__ == '__main__':
    start()
    sort_data()
